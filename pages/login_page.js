var loginCommands = {

    loginPass: function (username, password) {
        //wait until username filed is visible in page , then start to enter username and password
        return this.waitForElementVisible('@useremail')
            .setValue('@useremail', username)
            .setValue('@userpassword', password)
            .click('@loginbutton')
    },
};

module.exports = {
    commands: [loginCommands],

    elements: {
        useremail: 'input[id=user_username]',
        userpassword: 'input[id=user_password]',
        loginbutton: 'input[name=commit]'
    },
}