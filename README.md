# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* this repo for automate front end regression tests for shopping websites
* Version 1.0

### How do I get set up? ###

* Clone the repo
* Install node js latest version
* navigate to roth folder
* run these commands : 
* sudo npm install npm install nightwatch --save-dev -g
* npm install chromedriver --save-dev

* How to run tests
* nightwatch [source] [options] or ./node_modules/.bin/nightwatch [source] [options]

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact