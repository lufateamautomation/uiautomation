//Valid Browsers use BROWSER=[chrome, firefox, 'internet explorer'] //chrome only one fully working on local
//IE currently only works on saucelabs

const chrome = require("chromedriver");
const seleniumServer = require("selenium-server");
var browser = process.env.BROWSER == undefined ? "chrome" : process.env.BROWSER;

module.exports = {
  src_folders: [
    "tests"
  ],

  page_objects_path: [
    "pages"
  ],

  "webdriver": {
    "start_process": true,
      "port": 9515,
      "server_path": "node_modules/.bin/chromedriver",
  },

  test_settings: {
    default: {
      selenium_port: 4444,
      selenium_host: "127.0.0.1",
      skip_testcases_on_fail: false,
      screenshots: {
        enabled: true,
        path: "./test_reports",
        on_failure: true,
        on_error: true,
      },
      desiredCapabilities: {
        browserName: browser,

        chromeOptions: {
          w3c: false,
        },
        alwaysMatch: {
          acceptInsecureCerts: true,
        },
      },
      globals: {
        waitForConditionTimeout: 20000,
      },
    },
  }
};
