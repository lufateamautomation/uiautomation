const url = 'https://lis.qa.genalyte.com/login';

module.exports = {
  'Unsuccesfull login and verify user navigate home page'(browser) {
    browser
      .url(url)
      .setValue('input[id=user_username]', 'qa.api@gmail.com')
      .setValue('input[id=user_password]', 'qtest1234')
      .click('input[name=commit]')
      .assert.containsText('#flash_alert', 'Invalid email or password')
      .pause(3000)
      .end();
  }
}