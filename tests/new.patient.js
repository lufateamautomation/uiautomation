const url = 'https://lis.qa.genalyte.com/login';

module.exports = {
  'Succesfully create new patinet'(browser) {
    browser
      .url(url)
      .setValue('input[id=user_username]', 'qa.api@gmail.com')
      .setValue('input[id=user_password]', 'qa123456')
      .click('input[name=commit]')
      .assert.titleContains('Dashboard | Genalyte LIS')
      .pause(3000)
      .click("#navbarSupportedContent > ul > li:nth-child(4) > a")
      .assert.titleContains('Patients | Genalyte LIS')
      .pause(2000)
      .click('body > main > div > div.float-right.mb-2 > a')
      .pause(2000)
      .setValue('input[id=patient_first_name]', 'xaniar')
      .setValue('input[id=patient_last_name]', 'smith')
      .setValue('input[id=patient_prefix]', 'Dr')
      .setValue('input[id=patient_email]', 'qa.api@gmail.com')
      .setValue('input[id=patient_dob]','1984/12/25')
      .click("#new_patient > div.card > div > div.form-group.select.required.patient_gender > div > button > div > div > div")
      .click("#new_patient > div.card > div > div.form-group.select.required.patient_gender > div > div > div > ul > li:nth-child(2) > a > span.text")
      .click("#new_patient > div.card > div > div.form-group.select.optional.patient_ethnicity > div > button > div > div > div")
      .click("#new_patient > div.card > div > div.form-group.select.optional.patient_ethnicity > div > div > div > ul > li:nth-child(3) > a > span.text")
      .click("#new_patient > div.card > div > div.form-group.select.required.patient_race > div > button > div > div > div")
      .click("#new_patient > div.card > div > div.form-group.select.required.patient_race > div > div > div > ul > li:nth-child(5) > a > span.text")
      .setValue('input[id=patient_phone_numbers_home]', '(321)4578989')
      .setValue('input[id=patient_phone_numbers_mobile]', '(818)4578989')
      .setValue('input[id=patient_address_address1]', '211 Brayan Ave')
      .setValue('input[id=patient_address_city]', 'Irvine')
      .click("#new_patient > div.card > div > div:nth-child(14) > div > div > div.col.col-6.col-md-4.p-0.pr-1.state-dropdown > div > div > button > div > div > div")
      .click("#new_patient > div.card > div > div:nth-child(14) > div > div > div.col.col-6.col-md-4.p-0.pr-1.state-dropdown > div > div > div > div > ul > li:nth-child(7) > a > span.text")
      .setValue('input[id=patient_address_zip]', '92603')
      .click("#patient_address_country")
      .click("#patient_address_country > option:nth-child(238)")
      .click('#new_patient > div.card-footer > div > div:nth-child(1) > button')
      .assert.containsText('#flash_notice', 'Successfully created patient')
      .pause(3000)
      .end();
  }
}