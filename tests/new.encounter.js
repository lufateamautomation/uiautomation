const url = 'https://lis.qa.genalyte.com/login';

module.exports = {
  'Creat New Encounter form'(browser) {
    browser
      .url(url)
      .setValue('input[id=user_username]', 'qa.api@gmail.com')
      .setValue('input[id=user_password]', 'qa123456')
      .click('input[name=commit]')
      .assert.titleContains('Dashboard | Genalyte LIS')
      .pause(3000)
      .click("#navbarSupportedContent > ul > li:nth-child(3) > a")
      .assert.titleContains('Encounters | Genalyte LIS')
      .pause(2000)
      .click('body > main > div > div.float-right.mb-2 > a')
      .pause(2000)
       //click on Case Type
      .click("#new_encounter > div:nth-child(4) > div > div > div.form-group.select.required.encounter_case_type_id > div > button > div > div > div")
      .click('option[value="8563877c-a561-4f7d-91c4-041eb81e6c13"]')
      //click on System (San Diego)
      .click("#new_encounter > div:nth-child(4) > div > div > div.form-group.select.required.encounter_system_id > div > button > div > div > div")
      .click('option[value="3bd51c99-41ce-47e6-8138-2d0bd2f811f1"]')
      //click on Patient (search for Name)
      .click("#new_encounter > div:nth-child(4) > div > div > div.form-group.select.required.encounter_patient_id > div > button > div > div > div")
      .setValue('input[placeholder="Search..."]', 'peter brown')
      .useXpath().click("(//*[contains(text(),'peter brown')])")
      .pause(2000)
      //Come back to the Css from Xpath
      .useCss()
      //click on Provider (search for provider name)
      .click("#new_encounter > div:nth-child(4) > div > div > div.form-group.select.required.encounter_provider_id > div > button > div > div > div")
      .click('option[value="6b40ab08-b446-45b2-902d-c7de8cfea2df"]')
      //click on Facility
      .click("#new_encounter > div:nth-child(4) > div > div > div.form-group.select.optional.encounter_facility_id > div > button > div > div > div")
      .click('option[value="586b69d9-33a5-40ab-858d-1435e97f5091"]')
      //click on Add Panel
      .click("#new_encounter > div:nth-child(5) > div > div > div.form-group.select > div > button > div > div > div")
      .pause(3000)
      .useXpath().setValue('//*[@id="new_encounter"]/div[2]/div/div/div[1]/div/div/div[1]/input', "SARS")
      .useXpath().click("(//*[contains(text(),'Maverick SARS-CoV-2 Multi-Antigen Serology Panel (v2)')])")
      .pause(2000)
      //Come back to the Css from Xpath
      .useCss()
      //click on Add Speimen Button
      .click("#new_encounter > div:nth-child(7) > div > div > a")
      .useXpath().setValue("(//*[contains(@name,'specimens_attributes')])", 'GL89676543')
      //Come back to the Css from Xpath
      .useCss()
      .click("#specimen-collected-at-")
      .click("body > div.daterangepicker.ltr.single.opensright.show-calendar > div.drp-buttons > button.applyBtn.btn.btn-sm.btn-primary")
      .click("#new_encounter > div.card-footer > div > div:nth-child(1) > button")
      .assert.containsText('#flash_notice', 'Successfully created encounter')
      .pause(3000)
      .end();
  }
}
