const url = 'https://lis.qa.genalyte.com/login';

module.exports = {
  'Succesfull login and verify user navigate home page'(browser) {

    //define loginpage variable to access login page and all methods on that file
    const loginpage = browser.page.login_page();
    const validusername = 'qa.api@gmail.com';
    const validpassword = 'qa123456';

    browser.url(url);
    //call the varibale , and then use . notation to access the method of file
    loginpage.loginPass(validusername, validpassword);
    //switch again to browser method
    browser
    .assert.titleContains('Dashboard | Genalyte LIS')
    .pause(3000)
    .end();
  },
}